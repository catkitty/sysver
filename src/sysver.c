#include <stdio.h>
#include <windows.h>
#ifdef WIN32
#include <string.h>
#define HINST_T HINSTANCE
#else
#define HINST_T HANDLE
#endif

typedef union {
  struct {
    BYTE major, minor;
    WORD build;
  } s;
  DWORD dw;
} VersionInfo;

#ifdef WIN32
typedef struct {
  DWORD dwOSVersionInfoSize;
  DWORD dwMajorVersion;
  DWORD dwMinorVersion;
  DWORD dwBuildNumber;
  DWORD dwPlatformId;
  CHAR szCSDVersion[128];
  WORD wServicePackMajor;
  WORD wServicePackMinor;
  WORD wSuiteMask;
  BYTE wProductType;
  BYTE wReserved;
} VersionInfoEx;

typedef enum {
  V_WINDOWS,
  V_WINDOWS_95, V_WINDOWS_95OSR1, V_WINDOWS_95OSR2, V_WINDOWS_95OSR21, V_WINDOWS_95OSR25,
  V_WINDOWS_96,
  V_WINDOWS_98, V_WINDOWS_98SE,
  V_WINDOWS_ME,

  V_NT,
  V_NT_31, V_NT_35, V_NT_351,
  V_NT_4,
  V_2000, V_XP, V_2003,
  V_VISTA, V_7, V_8, V_81, V_TP,
  V_10
} WinVersion;

typedef BOOL(__stdcall *pgetvex)(PVOID);
typedef LPCSTR(__cdecl *pgetwine)();
typedef DWORD(__stdcall *pgetfvis)(LPCTSTR, LPDWORD);
typedef BOOL(__stdcall *pgetfvi)(LPCTSTR, DWORD, DWORD, LPVOID);
typedef BOOL(__stdcall *pverqv)(LPCVOID, LPCTSTR, LPVOID, PUINT);

const char* GetBrandName(WinVersion ver) {
  const char* name[] = {
    "",
    "95", "95OSR1", "95OSR2", "95OSR2.1", "95OSR2.5",
    "96",
    "98", "98SE",
    "Me",

    "NT",
    "NT 3.1", "NT 3.5", "NT 3.51",
    "NT 4",
    "2000", "XP", "Server 2003",
    "Vista", "7", "8", "8.1", "Technical Preview",
    "10"
  };
  if(ver > V_10) return name[V_WINDOWS];
  return name[ver];
}

WinVersion GetVersionId(VersionInfoEx* v) {
  if(v->dwPlatformId != VER_PLATFORM_WIN32_NT) {
    if(v->dwMajorVersion != 4) return V_WINDOWS;

    switch(v->dwMinorVersion) {
      case 0:                            return V_WINDOWS_95;
      case 10: if(v->szCSDVersion[0])    return V_WINDOWS_98SE;
        return V_WINDOWS_98;
      case 90:                           return V_WINDOWS_ME;
      default: return V_WINDOWS;
    }
  }

  switch(v->dwMajorVersion) {
    case 3: if(v->dwMinorVersion == 10) return V_NT_31;
      if(v->dwMinorVersion == 50) return V_NT_35;
      if(v->dwMinorVersion == 51) return V_NT_351;
      return V_NT;
    case 4: if(v->dwMinorVersion == 0)  return V_NT_4;
      return V_NT;
    case 5: if(v->dwMinorVersion == 0)  return V_2000;
      if(v->dwMinorVersion == 1)  return V_XP;
      if(v->dwMinorVersion == 2)  return V_2003;
      return V_NT;
    case 6: if(v->dwMinorVersion == 0)  return V_VISTA;
      if(v->dwMinorVersion == 1)  return V_7;
      if(v->dwMinorVersion == 2)  return V_8;
      if(v->dwMinorVersion == 3)  return V_81;
      if(v->dwMinorVersion == 4)  return V_TP;
      return V_NT;
    case 10:
      if(v->dwMinorVersion == 0)         return V_10;
      return V_10;
    default: return V_NT;
  }
}
#endif

#define SV_FAMILY 1
#define SV_NAME   2
#define SV_BUILD  4
#define SV_SPACK  8
#define SV_UA     16

#define SV_STYLE_FULL SV_FAMILY | SV_NAME | SV_BUILD | SV_SPACK
#define SV_STYLE_VER  SV_BUILD
#define SV_STYLE_UA   SV_FAMILY | SV_UA

#if __GNUC__
int GetSystemNameString(char* str, int style)
#else
int GetSystemNameString(str, style)
char* str;
int style;
#endif
{
  int len = 0;
  VersionInfo v;
#ifdef WIN32
  VersionInfoEx ntv;
  WinVersion verid;

  HINST_T kernel32 = LoadLibrary("kernel32.dll");
  HINST_T ntdll = LoadLibrary("ntdll.dll");
  HINST_T winver = LoadLibrary("version.dll");

  LPSTR ROSver;

  if(kernel32) {                                                   // Jeśli Win32
    pgetvex getvex = (pgetvex)GetProcAddress(kernel32, "GetVersionExA");

    if(!getvex) {                                     // Jeśli brak GetVersionEx()
#endif
      if(style & SV_FAMILY) len = sprintf(str, "Windows ");
      v.dw = GetVersion();
      if(v.s.minor < 95) {
        if(v.s.minor % 10)
          len = sprintf(str, "%d.%02d", v.s.major, v.s.minor);
        else
          len = sprintf(str, "%d.%d", v.s.major, v.s.minor / 10);
      } else if(style & SV_NAME) len += sprintf(str + len, "on Windows");
#ifdef WIN32
    } else {                                         // Jeśli jest GetVersionEx()
      memset(&ntv, 0, sizeof(OSVERSIONINFO));
      ntv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
      getvex(&ntv);

      verid = GetVersionId(&ntv);
      if(ntv.dwPlatformId != VER_PLATFORM_WIN32_NT) {              // Jeśli nie NT
        if(style & SV_FAMILY) len = sprintf(str, "Windows ");
        if(!verid || (!(style & SV_NAME) && !(style & SV_UA)))
          len += sprintf(str + len, "%d.%d",
            ntv.dwMajorVersion,
            ntv.dwMinorVersion % 10 ? ntv.dwMinorVersion : ntv.dwMinorVersion / 10
          );
        else {
          if(verid == V_WINDOWS_98SE) verid = V_WINDOWS_98;
          len += sprintf(str + len, "%s", GetBrandName(verid));
        }
        if(style & SV_BUILD)
          if(ntv.dwBuildNumber < 0x8000)      // Czy numer kompilacji jest poprawny?
            if(style & SV_NAME)
              len += sprintf(str + len, " (build %d)", ntv.dwBuildNumber);
            else len += sprintf(str + len, ".%d", ntv.dwBuildNumber);
      } else if(ntdll) {                                     // Jeśli platforma NT
        pgetwine wine_get_version;
        wine_get_version = (pgetwine)GetProcAddress(ntdll, "wine_get_version");
        if(wine_get_version) {                                        // Jeśli Wine
          if(style & SV_FAMILY) len += sprintf(str, "Wine ");
          len += sprintf(str + len, "%s", wine_get_version());
        } else {
          memset(&ntv, 0, sizeof(VersionInfoEx));
          ntv.dwOSVersionInfoSize = sizeof(VersionInfoEx);
          getvex(&ntv);

          /* TODO:Procedura wykrywania ReactOS                                    */
          ROSver = ntv.szCSDVersion;
          while(*(ROSver++));
          ROSver++;
          if(strncmp(ROSver, "ReactOS", 7) == 0) {                  // Jeśli ReactOS
            if(!(style & SV_FAMILY)) ROSver += 8;
            len = sprintf(str, "%s", ROSver);
          } else {                                                     // Windows NT
            if(style & SV_FAMILY) len = sprintf(str, "Windows ");
            if(verid == V_NT || !(style & SV_NAME)) {
              if(style & SV_FAMILY) len += sprintf(str + len, "NT ");
              len += sprintf(str + len, "%d.%d",
                ntv.dwMajorVersion,
                ntv.dwMinorVersion % 10 ? ntv.dwMinorVersion : ntv.dwMinorVersion / 10
              );
            } else len += sprintf(str + len, "%s", GetBrandName(verid));
            if(style & SV_BUILD)
              if(ntv.dwBuildNumber < 0x8000)    // Czy numer kompilacji jest poprawny?
                if(style & SV_NAME)
                  len += sprintf(str + len, " (build %d)", ntv.dwBuildNumber);
                else len += sprintf(str + len, ".%d", ntv.dwBuildNumber);
                if(style & SV_SPACK) if(ntv.szCSDVersion[0])
                  len += sprintf(str + len, ", %s", ntv.szCSDVersion);
          }
        }
      }
    }
  }
#endif
  return len;
}

#if __GNUC__
int WINAPI WinMain(HINST_T hInstance, HINST_T hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
#else
int PASCAL WinMain(hInstance, hPrevInstance, lpCmdLine, nCmdShow)
HINST_T hInstance, hPrevInstance;
LPSTR lpCmdLine;
int nCmdShow;
#endif
{
  char buff[256];
  int n = GetSystemNameString(buff, SV_STYLE_FULL);
  buff[n] = '\n';
  n++;
  n += GetSystemNameString(buff + n, SV_STYLE_VER);
  buff[n] = '\n';
  n++;
  n += GetSystemNameString(buff + n, SV_STYLE_UA);
  MessageBox((HWND)NULL, buff, "Matriksoft Sysver", MB_OK);
  return 0;
}
