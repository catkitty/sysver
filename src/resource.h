#define SYSVER_VERSION 1,0,57,0
#define SYSVER_VERSION_S "1.0.57\0"

#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#ifndef VOS_WINDOWS16
#define VOS_WINDOWS16 1
#define VOS_WINDOWS32 4
#endif

#ifndef VFT_APP
#define VFT_APP 1
#endif
