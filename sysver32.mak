# Microsoft Visual C++ Generated NMAKE File, Format Version 2.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

!IF "$(CFG)" == ""
CFG=Win32 Debug
!MESSAGE No configuration specified.  Defaulting to Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "Win32 Release" && "$(CFG)" != "Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE on this makefile
!MESSAGE by defining the macro CFG on the command line.  For example:
!MESSAGE 
!MESSAGE NMAKE /f "sysver32.mak" CFG="Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

################################################################################
# Begin Project
# PROP Target_Last_Scanned "Win32 Debug"
MTL=MkTypLib.exe
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "WinRel"
# PROP BASE Intermediate_Dir "WinRel"
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "bin"
# PROP Intermediate_Dir "release/sysver32"
OUTDIR=.\bin
INTDIR=.\release/sysver32

ALL : $(OUTDIR)/sysver32.exe .\release\sysver32\sysver32.bsc

$(OUTDIR) : 
    if not exist $(OUTDIR)/nul mkdir $(OUTDIR)

$(INTDIR) : 
    if not exist $(INTDIR)/nul mkdir $(INTDIR)

# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /win32
MTL_PROJ=/nologo /D "NDEBUG" /win32 
# ADD BASE CPP /nologo /W3 /GX /YX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FR /c
# ADD CPP /nologo /W3 /GX /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /c
CPP_PROJ=/nologo /W3 /GX /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Fo$(INTDIR)/\
 /c 
CPP_OBJS=.\release/sysver32/
# ADD BASE RSC /l 0x415 /d "NDEBUG"
# ADD RSC /l 0x415 /d "NDEBUG"
RSC_PROJ=/l 0x415 /fo$(INTDIR)/"sysver32.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_SBRS= \
	
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /o"release/sysver32/sysver32.bsc"
BSC32_FLAGS=/nologo /o"release/sysver32/sysver32.bsc" 

.\release\sysver32\sysver32.bsc : $(OUTDIR)  $(BSC32_SBRS)
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /NOLOGO /SUBSYSTEM:windows /MACHINE:I386
# ADD LINK32 kernel32.lib user32.lib /NOLOGO /VERSION:1,0 /SUBSYSTEM:windows /PDB:none /MACHINE:I386
LINK32_FLAGS=kernel32.lib user32.lib /NOLOGO /VERSION:1,0 /SUBSYSTEM:windows\
 /PDB:none /MACHINE:I386 /OUT:$(OUTDIR)/"sysver32.exe" 
DEF_FILE=
LINK32_OBJS= \
	$(INTDIR)/sysver.obj \
	$(INTDIR)/sysver32.res

$(OUTDIR)/sysver32.exe : $(OUTDIR)  $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "WinDebug"
# PROP BASE Intermediate_Dir "WinDebug"
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "bin"
# PROP Intermediate_Dir "debug/sysver32"
OUTDIR=.\bin
INTDIR=.\debug/sysver32

ALL : $(OUTDIR)/sysver32_debug.exe .\debug\sysver32\sysver32.bsc

$(OUTDIR) : 
    if not exist $(OUTDIR)/nul mkdir $(OUTDIR)

$(INTDIR) : 
    if not exist $(INTDIR)/nul mkdir $(INTDIR)

# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /win32
MTL_PROJ=/nologo /D "_DEBUG" /win32 
# ADD BASE CPP /nologo /W3 /GX /Zi /YX /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /c
# ADD CPP /nologo /W3 /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Fd"debug/sysver32/sysver32.pdb" /c
CPP_PROJ=/nologo /W3 /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS"\
 /Fo$(INTDIR)/ /Fd"debug/sysver32/sysver32.pdb" /c 
CPP_OBJS=.\debug/sysver32/
# ADD BASE RSC /l 0x415 /d "_DEBUG"
# ADD RSC /l 0x415 /d "_DEBUG"
RSC_PROJ=/l 0x415 /fo$(INTDIR)/"sysver32.res" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_SBRS= \
	
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /o"debug/sysver32/sysver32.bsc"
BSC32_FLAGS=/nologo /o"debug/sysver32/sysver32.bsc" 

.\debug\sysver32\sysver32.bsc : $(OUTDIR)  $(BSC32_SBRS)
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /NOLOGO /SUBSYSTEM:windows /DEBUG /MACHINE:I386
# ADD LINK32 kernel32.lib user32.lib /NOLOGO /VERSION:1,0 /SUBSYSTEM:windows /PDB:none /DEBUG /MACHINE:I386 /OUT:"bin/sysver32_debug.exe"
LINK32_FLAGS=kernel32.lib user32.lib /NOLOGO /VERSION:1,0 /SUBSYSTEM:windows\
 /PDB:none /DEBUG /MACHINE:I386 /OUT:"bin/sysver32_debug.exe" 
DEF_FILE=
LINK32_OBJS= \
	$(INTDIR)/sysver.obj \
	$(INTDIR)/sysver32.res

$(OUTDIR)/sysver32_debug.exe : $(OUTDIR)  $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

################################################################################
# Begin Group "Source Files"

################################################################################
# Begin Source File

SOURCE=.\src\sysver.c

$(INTDIR)/sysver.obj :  $(SOURCE)  $(INTDIR)
   $(CPP) $(CPP_PROJ)  $(SOURCE) 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\src\sysver32.rc
DEP_SYSVE=\
	.\src\resource.h

!IF  "$(CFG)" == "Win32 Release"

$(INTDIR)/sysver32.res :  $(SOURCE)  $(DEP_SYSVE) $(INTDIR)
   $(RSC) /l 0x415 /fo$(INTDIR)/"sysver32.res" /i "src" /d "NDEBUG"  $(SOURCE) 

!ELSEIF  "$(CFG)" == "Win32 Debug"

$(INTDIR)/sysver32.res :  $(SOURCE)  $(DEP_SYSVE) $(INTDIR)
   $(RSC) /l 0x415 /fo$(INTDIR)/"sysver32.res" /i "src" /d "_DEBUG"  $(SOURCE) 

!ENDIF 

# End Source File
# End Group
# End Project
################################################################################
