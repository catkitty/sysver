var cin = WScript.StdIn;
var cout = WScript.StdOut;

var name = "unknown";
var build = 0;

var pat_num = /#define\s+([A-Z_]+)_VERSION\s+(\d+,\d+),(\d+),0/;
var pat_str = /#define\s+([A-Z_]+)_VERSION_S\s+"(\d+.\d+).(\d+)\\0"/;

while(!cin.AtEndOfStream) {
  var str = cin.ReadLine();
  if(pat_num.test(str)) {
    var m = pat_num.exec(str);
    
    name = m[1];
    build = Math.round(m[3]);
    build++;
    cout.WriteLine("#define " + m[1] + "_VERSION " + m[2] + "," + build + ",0");
  } else if(pat_str.test(str)) {
    var m = pat_str.exec(str);
    
    if(m[1] == name)
      cout.WriteLine("#define " + m[1] + "_VERSION_S \"" + m[2] + "." + build + "\\0\"");
  } else cout.WriteLine(str);
}
